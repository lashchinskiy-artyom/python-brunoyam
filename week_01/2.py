'''
Написать скрипт, который будет:

1. Записывать в созданный файл три числа в 3 строки
2. Получать записанные данные
3. Формировать записанные данные в список
'''

filepath = './week_01/some_file.txt'

num1 = input("Введите первое число: ")
num2 = input("Введите второе число: ")
num3 = input("Введите третье число: ")

outputFile = open(filepath, "w")
outputFile.write(num1 + '\n')
outputFile.write(num2 + '\n')
outputFile.write(num3 + '\n')
outputFile.close()

inputFile = open(filepath, "r")
lines = inputFile.read()
inputFile.close()

list = []
for line in lines:
  if line != '\n':
    list.append(line)

print(type(list), list)