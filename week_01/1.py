'''
Создать функцию, которая будет принимать аргументом строку, 
внутри функции должен выполняться цикл for для данной строки.
И если в строке есть буква А, то выводить ее в консоль(Делать принт)
'''

def printA(string: str):
  for char in string:
    if char == "a":
      print(char)
    elif char == "A":
      print(char)
    elif char == "а":
      print(char)
    elif char == "А":
      print(char)

string = input("Введите какой-нибудь текст: ")
printA(string)