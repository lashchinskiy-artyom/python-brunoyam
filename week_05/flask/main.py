from flask import Flask, request, render_template, redirect
import sqlite3

app = Flask(__name__)
DB = './db.sqlite'

def get_db_connection():
    conn = sqlite3.connect(DB)
    conn.row_factory = sqlite3.Row
    return conn

def query_db(query, args=(), one=False):
    cur = get_db_connection().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

@app.route('/', methods=['GET'])
def index():
    return render_template("index.html")

@app.route('/login', methods=['GET'])
def loginShow():
    return render_template("login.html")

@app.route('/login', methods=['POST'])
def loginCheck():
    user = request.form['username']
    pswd = request.form['password']

    connect = get_db_connection()
    cursor = connect.cursor()

    query = "SELECT username, password FROM users WHERE username = '{u}' AND password = '{p}';".format(u = user, p = pswd)
    rows = cursor.execute(query)
    rows = rows.fetchall()
    print(len(rows))

    if len(rows) == 0:
        error = 'User/Password not found'
        return render_template("login.html", error=error)
    elif len(rows) == 1:
        success = 'You successfully login'
        return redirect('dashboard')
    

@app.route('/register', methods=['GET'])
def registerShow():
    return render_template("register.html")

@app.route('/register', methods=['POST'])
def registerStore():
    user = request.form['username']
    pswd = request.form['password']

    connect = get_db_connection()
    cursor = connect.cursor()

    rows = query_db('select username from users where username = ?',
                [user], one=True)
    
    if rows is None:
        query = "INSERT INTO users (username, password) VALUES ('{u}', '{p}')".format(u = user, p = pswd)
        cursor.execute(query)
        connect.commit()
        connect.close()

        success = "User registered"
        return render_template("register.html", success=success)
    elif len(rows) == 1:
        error = "Username exists"
        return render_template("register.html", error=error) 

@app.route('/dashboard', methods=['GET'])
def dashboard():
  return render_template("dashboard.html")

app.run(host='0.0.0.0', port='8800', debug=True)