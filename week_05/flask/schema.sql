DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username NVARCHAR(32) NOT NULL,
    password NVARCHAR(32) NOT NULL
);

INSERT INTO users (username, password) VALUES ('root', 'P@ssw0rd');