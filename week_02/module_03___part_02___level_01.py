'''
Модуль 03 - Основы Python II

Часть 2. Уровень 1.

Дан произвольный список.

Для примера: l = [1, 4, 1, 6, "hello", "a", 5, "hello"]. Найдите в нём все повторяющиеся элементы.
Можно использовать цикл for. Найденные повторяющиеся элементы, стоит добавлять в новый список, чтобы потом вывести на экран вместе
'''

list = [1, 4, 1, 6, "hello", "a", 5, "hello"]
doubleItems = []

for k, v in enumerate(list):
    for i in range(k + 1, len(list)):
        if list[i] == list[k]:
            doubleItems.append(v)

print(doubleItems)