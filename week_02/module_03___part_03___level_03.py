'''
Модуль 03 - Основы Python II

Часть 3. Уровень 3.

У вас есть массив чисел, напишите функцию, которая составляет из них максимальное число

Пример:
[56, 9, 11, 2] -> 956211
[3, 81, 5] -> 8153
'''

def listToMaxNumber(list: list) -> int:
    maxNumberList = []
    maxNumberStr = ''
    while len(list) != 0:
        maxNum = 0
        index = 0
        for item in list:
            maxBefore = maxNum
            maxNum = max(maxNum, int(str(item)[0]))
            if maxNum != maxBefore:
                index = list.index(item)
        maxNumberList.append(list[index])
        list.pop(index)
    
    for num in maxNumberList:
        maxNumberStr += str(num)

    return int(maxNumberStr)

def max(first: int, second: int) -> int:
    if first > second:
        return first
    return second

listOne = [56, 9, 11, 2]
listTwo = [3, 81, 5]
listThree = [18, 27, 36, 45, 54, 63, 72, 81, 90]

print(listToMaxNumber(listOne))
print(listToMaxNumber(listTwo))
print(listToMaxNumber(listThree))