'''
Модуль 03 - Основы Python II

Часть 2. Уровень 3.

Дан словарь. Поменяйте в нём местами ключи и значения
d = {'name1': 'id1', 'name2': 'id2', 'name3': 'id3'}
'''

dict0 = {'name1': 'id1', 'name2': 'id2', 'name3': 'id3'}

dict1 = {}

for k in dict0.keys():
    dict1[dict0[k]] = k

print(dict0, dict1)