'''
Модуль 01 - Основы Python I

Часть 2. Уровень 1.

Даны 3 целых числа. Определите, сколько среди них совпадающих. 
Программа должна вывести одно из чисел: 3 (если все совпадают),
2 (если 2 совпадает), 0 (если все числа различны)
'''

num1 = int(input('Введите первое целое число: '))
num2 = int(input('Введите второе целое число: '))
num3 = int(input('Введите третье целое число: '))

if num1 == num2 == num3:
  print(3)
elif num1 == num2 and num1 != num3 and num2 != num3:
  print(2)
elif num2 == num3 and num2 != num1 and num3 != num1:
  print(2)
elif num3 == num1 and num3 != num2 and num1 != num2:
  print(2)
else:
  print(0)