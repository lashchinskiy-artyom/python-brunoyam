'''
Модуль 01 - Основы Python I

Часть 2. Уровень 2.

Пусть пользователь придумывает пароль и вводит его с клавиатуры. Допустимым 
считается пароль, который состоит более чем из 8 символов и включает как 
прописные, так и заглавные буквы. 
Используя цикл while дождитесь, пока пользователь введет допустимый пароль. 
Длину строки можно считать с помощью функции len() .
'''

allowSymbols = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
passTrue = None

while passTrue != True:
  passTrue = None
  password = input("Придумайте пароль (не менее 8 символов из букв разного регистра): ")
  if (len(password)) >= 8:
    for char in password:
      if passTrue == None or passTrue == True:
        for symbol in allowSymbols:
          if char == symbol:
            passTrue = True
            break
          else:
            passTrue = False
            continue
      elif passTrue == False:
        break
  
  if passTrue == True:
    print('Пароль установлен')
  