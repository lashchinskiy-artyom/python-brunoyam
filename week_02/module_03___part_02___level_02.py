'''
Модуль 03 - Основы Python II

Часть 2. Уровень 2.

Дана матрица (список из вложенных списков) размера N x N, найдите в ней максимальный элемент
'''

from random import randint

def max(first: int, second: int) -> int:
    if first >= second:
        return first
    return second

n = 5
m = [[randint(0, 100) for i in range(n)] for j in range(n)]

print(m)

maxI = 0
maxJ = 0

for i, v1 in enumerate(m):
    for j, v2 in enumerate(m[i]):
        if j < len(m[i]):
            maxJ = max(maxJ, m[i][j])
    maxI = max(maxI, maxJ)

print(maxI)