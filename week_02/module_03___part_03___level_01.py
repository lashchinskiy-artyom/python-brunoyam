'''
Модуль 03 - Основы Python II

Часть 3. Уровень 1.

Напишите функцию, которая считает площадь треугольника по трём сторонам

def area(a, b, c):
    pass
'''

import math

def area(a: float, b: float, c: float) -> float:
    hp = halfPerimeter(a, b, c)
    return math.sqrt(hp * (hp - a) * (hp - b) * (hp - c))

def halfPerimeter(a: float, b: float, c: float) -> float:
    return (a + b + c) / 2

area = area(3, 4, 5)
print(area)