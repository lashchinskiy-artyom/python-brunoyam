'''
Модуль 03 - Основы Python II

Часть 4. Уровень 1.

Пусть пользователь вводит логин и пароль для регистрации. Напишите функцию, которая принимает логин и пароль в качестве аргументов и записывает в json-файл.

Подумайте над тем, в каких структурах данных лучше хранить данную информацию
'''
import json

def register(login: str, password: str) -> None:
    json_data = readFile()
    json_data[login] = password
    file = './week_02/users.json'
    with open(file, 'w') as outfile:
        json.dump(json_data, outfile)
    return False

def readFile() -> dict:
    file = './week_02/users.json'
    data = open(file, 'r')
    return json.load(data)

user1_login = 'user1'
user1_password = 'P@SsW0rD'
user2_login = 'user2'
user2_password = 'P@SsW0rD'
user3_login = 'user3'
user3_password = 'P@SsW0rD'
user4_login = 'user4'
user4_password = 'P@SsW0rD'

register(user1_login, user1_password)
# register(user2_login, user2_password)
# register(user3_login, user3_password)
# register(user4_login, user4_password)